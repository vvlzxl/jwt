package com.zxl.jwt.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.stereotype.Component;

/**
 * @author 良子
 * @title: MyBeanPostProcessor
 * @projectName jwt
 * @description: TODO
 * @date 2021/3/9 16:59
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    /**
     * 前置处理器
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    /**
     * 后置处理器
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof DaoAuthenticationProvider) {
            ((DaoAuthenticationProvider) bean).setHideUserNotFoundExceptions(false);
        }
        return bean;
    }
}
