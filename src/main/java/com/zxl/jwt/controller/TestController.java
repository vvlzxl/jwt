package com.zxl.jwt.controller;

import com.zxl.jwt.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 良子
 * @title: TestController
 * @projectName jwt
 * @description: TODO
 * @date 2021/3/9 14:43
 */
@RestController
public class TestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @PostMapping("/login")
    public Map<String,String> login(String username,String password){
        Map<String, String> map = new HashMap<>();
        //创建用户名密码令牌 (如果想带验证码登录 可以继承该类重写即可)
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username,password);
        //将令牌传给认证管理器进行认证
        Authentication authenticate = authenticationManager.authenticate(token);
        //生成token
        String token1 = tokenService.getToken(authenticate);
        map.put("token",token1);
        return map;
    }

    @RequestMapping("/test")
    public String test(){
        return "获取成功";
    }
}
