package com.zxl.jwt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * 用户信息服务类
 * @author 良子
 * @title: MyUserDetailsServiceImpl
 * @projectName jwt
 * @description: TODO
 * @date 2021/3/9 15:38
 */
@Component
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        /*
          这里进行硬编码了用户名 实际项目中可以通过用户名去数据库查询用户信息进行判断
         */
        String localUsername = "admin";
        if(!localUsername.equals(username)){
            throw new UsernameNotFoundException("用户不存在");
            //自定义异常绕开security的隐藏机制
        }
        //简单权限(该用户拥有ROLE_USER权限)
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");

        //创建简单UserDetails对象返回给认证提供者  (实际项目我们可以实现UserDetails接口进行用户信息自定义例如禁用，锁用户等)
        return new User(localUsername, passwordEncoder.encode("123456"), Collections.singleton(authority));
    }
}
