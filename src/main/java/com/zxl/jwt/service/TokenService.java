package com.zxl.jwt.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author 良子
 * @title: TokenService
 * @projectName jwt
 * @description: TODO
 * @date 2021/3/9 18:51
 */
@Component
public class TokenService {

    /**
     * 获取加密算法
     */
    private Algorithm getSAlgorithm(){
        return Algorithm.HMAC256("secret");
    }

    /**
     * 生成token
     * @param authentication
     * @return
     */
    public String getToken(Authentication authentication) {
        String token = null;
        //过期时间30分钟
        Date date = new Date(System.currentTimeMillis() + 1800000);
        try {
            token = JWT.create()
                    .withIssuer("zxl")
                    .withExpiresAt(date)
                    .withClaim("username", authentication.getName())
                    .sign(this.getSAlgorithm());
        }catch (JWTCreationException e) {
            e.printStackTrace();
        }
        return token;
    }

    /**
     * 验证token合法性 （简单验证实际项目根据自己情况验证）
     */
    public boolean Verified(String token){
        try {
            JWTVerifier jwtVerifier = JWT.require(this.getSAlgorithm())
                    .withIssuer("zxl")
                    .build();
            DecodedJWT verify = jwtVerifier.verify(token);
            return true;
        }catch (JWTVerificationException e){
            System.out.println(e);
            return false;
        }
    }
}
