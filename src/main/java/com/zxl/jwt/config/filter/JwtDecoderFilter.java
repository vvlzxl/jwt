package com.zxl.jwt.config.filter;

import com.zxl.jwt.service.TokenService;
import javafx.beans.property.SimpleStringProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

/**
 * @author 良子
 * @title: JwtDecoderFilter
 * @projectName jwt
 * @description: TODO
 * @date 2021/3/9 19:21
 */
@Component
public class JwtDecoderFilter extends OncePerRequestFilter {

    @Autowired
    private TokenService tokenService;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //获取请求头中的token
        String authorization = request.getHeader("Authorization");
        System.out.println(authorization);
        if (authorization != null) {
            boolean verified = tokenService.Verified(authorization);
            System.out.println(verified);
            if (verified) {
                //实际项目中可以把登录成功的用户实体保存到redis通过token取到填充即可 （这里为了简便我直接硬编码了）
                SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("admin", 123456, Collections.singleton(authority));
                //将认证通过的信息填充到安全上下文中（用于一次请求的授权）
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request, response);
    }
}
