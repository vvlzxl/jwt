package com.zxl.jwt.config;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 良子
 * @title: MyAuthenticationEntryPoint
 * @projectName jwt
 * @description: TODO
 * @date 2021/3/9 16:22
 */
@Component
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        /**
         * 给用户返回信息（一种在这里直接拦截处理 ，一种抛出异常通过全局异常拦截器进行处理）
         */
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        Map<String,String> map =new HashMap<>();
        JsonMapper mapper = new JsonMapper();
        if (authException instanceof UsernameNotFoundException){
            response.setStatus(401);
            map.put("msg","用户名不存在");
            mapper.writeValue(response.getWriter(), map);
        }if (authException instanceof InsufficientAuthenticationException){
            response.setStatus(401);
            map.put("msg","用户未登录");
            mapper.writeValue(response.getWriter(), map);
        }
    }
}
